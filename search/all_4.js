var searchData=
[
  ['data_5flist_0',['data_list',['../Lab02__main_8py.html#a0b71a7a9a2cc2e10bd400d81dfa5976c',1,'Lab02_main.data_list()'],['../Lab03__main_8py.html#a30d0f868d841d913ed7b6d9584945424',1,'Lab03_main.data_list()'],['../Lab04__main_8py.html#ad6decc4b9567417367638bc7c5cbe3a6',1,'Lab04_main.data_list()']]],
  ['data_5flist_5f2_1',['data_list_2',['../Lab03__main_8py.html#a313c2b52e263dc7c24b448b640760ac1',1,'Lab03_main.data_list_2()'],['../Lab04__main_8py.html#a02500363413f584cd6710cc323daafec',1,'Lab04_main.data_list_2()']]],
  ['delta_2',['delta',['../Lab02__main_8py.html#af9dd824b1f723b91f744db59956ce357',1,'Lab02_main.delta()'],['../Lab03__main_8py.html#a15afbc7eaeaa5494138bb5e1afa2d7fa',1,'Lab03_main.delta()'],['../Lab04__main_8py.html#af0260d1cca04ffd9fdc17d852649b598',1,'Lab04_main.delta()']]],
  ['delta_5f2_3',['delta_2',['../Lab03__main_8py.html#ae5cc234ade087822c2a4935b629f2fa2',1,'Lab03_main.delta_2()'],['../Lab04__main_8py.html#ad8763757af0b1ce32cc6d0cd76926257',1,'Lab04_main.delta_2()']]],
  ['disable_4',['disable',['../classLab03__motordriver_1_1DRV8847.html#ae1007abc1d6360c6944de084404561a6',1,'Lab03_motordriver.DRV8847.disable()'],['../classLab04__motordriver_1_1DRV8847.html#af39a49f0c1918f2ff4f60d055197e71b',1,'Lab04_motordriver.DRV8847.disable()'],['../classTerm__motordriver_1_1DRV8847.html#a1465e0abe9ba62b8235a938182a89cc0',1,'Term_motordriver.DRV8847.disable()']]],
  ['drv8847_5',['DRV8847',['../classLab03__motordriver_1_1DRV8847.html',1,'Lab03_motordriver.DRV8847'],['../classLab04__motordriver_1_1DRV8847.html',1,'Lab04_motordriver.DRV8847'],['../classTerm__motordriver_1_1DRV8847.html',1,'Term_motordriver.DRV8847']]],
  ['duty_5fm1_6',['duty_m1',['../Lab03__main_8py.html#a8b59a94399333cdcf30f3c3522414a1f',1,'Lab03_main.duty_m1()'],['../Lab04__main_8py.html#a3db53e06fd766479a2ebdde068907cd6',1,'Lab04_main.duty_m1()']]],
  ['duty_5fm2_7',['duty_m2',['../Lab03__main_8py.html#a8708c37df8b58b7ea5924466e501d69e',1,'Lab03_main.duty_m2()'],['../Lab04__main_8py.html#a54cef88a6c6f6d8213f1573b43b47f65',1,'Lab04_main.duty_m2()']]]
];
