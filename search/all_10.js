var searchData=
[
  ['s0_5finit_0',['S0_Init',['../Lab03__task__motor_8py.html#a3e7619c7f447a2dca5ae9657fccfa7b7',1,'Lab03_task_motor.S0_Init()'],['../Lab04__task__motor_8py.html#aa5112fb32d33e14b314b7716d02543aa',1,'Lab04_task_motor.S0_Init()'],['../Term__task__controller_8py.html#ab9f2e3747e9d5c2211e7ef9e4a70169e',1,'Term_task_controller.S0_Init()'],['../Term__task__datacollection_8py.html#a0c6306b1b44b0bb8f978d64ca8b7f7d6',1,'Term_task_datacollection.S0_Init()'],['../Term__task__imu_8py.html#ac0f9191c7a838bea3870437b34fc50f6',1,'Term_task_imu.S0_Init()'],['../Term__task__motor_8py.html#a34a70d95b135dd23e75022f27e622392',1,'Term_task_motor.S0_Init()'],['../Term__task__touchpanel_8py.html#a5d7996fc8223f25213ed945342416c03',1,'Term_task_touchpanel.S0_Init()']]],
  ['s1_5fstopbalancing_1',['S1_StopBalancing',['../Term__task__controller_8py.html#aeb464dd67420773d67547883ebfbb791',1,'Term_task_controller']]],
  ['s1_5fupdate_2',['S1_Update',['../Lab03__task__motor_8py.html#af10e55ae84515ebf87c8526910e1eada',1,'Lab03_task_motor.S1_Update()'],['../Term__task__imu_8py.html#a8faef48a89a4b1fa2e8d06b9a293e8d4',1,'Term_task_imu.S1_Update()'],['../Term__task__datacollection_8py.html#a28891d5cdeb8d0d5f90758542c356eee',1,'Term_task_datacollection.S1_Update()'],['../Lab04__task__motor_8py.html#aed9abca852b97bbcc069e9f65378bc80',1,'Lab04_task_motor.S1_Update()'],['../Term__task__motor_8py.html#a05c475e3a13695937c372fb4d9e9b1ac',1,'Term_task_motor.S1_Update()'],['../Term__task__touchpanel_8py.html#a54c679fdef87e9085f9b4b4581f24a04',1,'Term_task_touchpanel.S1_Update()']]],
  ['s2_5fbalancing_3',['S2_Balancing',['../Term__task__controller_8py.html#ae61e5d8455c6524188ad828eeec2a170',1,'Term_task_controller']]],
  ['s2_5fcalibrate_4',['S2_Calibrate',['../Term__task__touchpanel_8py.html#ad6af6c66ab48a1351e7a1a346ada998c',1,'Term_task_touchpanel']]],
  ['s2_5fcalibration_5',['S2_Calibration',['../Term__task__imu_8py.html#a97ac760017565b542e29ceb3460af1bc',1,'Term_task_imu']]],
  ['s2_5fcollectdata_6',['S2_CollectData',['../Term__task__datacollection_8py.html#ab00a5597be0d29f5e4c6158c943e8bd9',1,'Term_task_datacollection']]],
  ['s3_5fwritefile_7',['S3_WriteFile',['../Term__task__touchpanel_8py.html#accfe1dd58908dfa004505bdaf17c7e63',1,'Term_task_touchpanel']]],
  ['set_5fduty_8',['set_duty',['../classLab03__motordriver_1_1Motor.html#abdc6d394505bc929d00c39389a762b84',1,'Lab03_motordriver.Motor.set_duty()'],['../classLab04__motordriver_1_1Motor.html#a5d8918700bcc50fb18dadd1820fb7561',1,'Lab04_motordriver.Motor.set_duty()'],['../classTerm__motordriver_1_1Motor.html#a751e92c4e39ea2a88753e3570d74d700',1,'Term_motordriver.Motor.set_duty()']]],
  ['set_5fk_9',['set_K',['../classTerm__closedloop_1_1ClosedLoop.html#a4d7f60aa145f69695a17fc6b8e90841b',1,'Term_closedloop::ClosedLoop']]],
  ['set_5fposition_10',['set_position',['../classLab02__encoder_1_1Encoder.html#aa80722ea56b828fbc1be4e1c2378f8cd',1,'Lab02_encoder.Encoder.set_position()'],['../classLab03__encoder_1_1Encoder.html#ab42a9bd84905348eeaf059912f57b3e2',1,'Lab03_encoder.Encoder.set_position()'],['../classLab04__encoder_1_1Encoder.html#a25cb4263fa5de31deec0aa5c26aa0b3b',1,'Lab04_encoder.Encoder.set_position()']]],
  ['share_11',['Share',['../classLab02__shares_1_1Share.html',1,'Lab02_shares.Share'],['../classLab03__shares_1_1Share.html',1,'Lab03_shares.Share'],['../classLab04__shares_1_1Share.html',1,'Lab04_shares.Share'],['../classTerm__shares_1_1Share.html',1,'Term_shares.Share']]],
  ['simulation_20of_20the_20ball_20balancer_12',['Simulation of the Ball Balancer',['../Simulation.html',1,'']]],
  ['source_20code_13',['Source Code',['../SourceCode.html',1,'']]],
  ['start_5fdata_5fcollection_14',['start_data_collection',['../Term__main_8py.html#a72cb73ea134f071209117cafdd4dfd54',1,'Term_main']]],
  ['state_15',['state',['../Lab01_8py.html#a89539619c7e7ef4431edeced6eed7b6b',1,'Lab01']]],
  ['stepresp_5factuationlevel_5fdata_16',['stepresp_actuationlevel_data',['../Lab04__main_8py.html#a43bbdece6a31725527c086cf78277ed5',1,'Lab04_main']]],
  ['stepresp_5factuationlevel_5fdata_5f2_17',['stepresp_actuationlevel_data_2',['../Lab04__main_8py.html#a14d1d72774affe035247b98b8822458f',1,'Lab04_main']]],
  ['stepresp_5fencoder_5fdata_18',['stepresp_encoder_data',['../Lab04__main_8py.html#ab82c875d550cfe294a71d9523088228b',1,'Lab04_main']]],
  ['stepresp_5fencoder_5fdata_5f2_19',['stepresp_encoder_data_2',['../Lab04__main_8py.html#ae46bd5abbad4ca17a71cefbdd37f36bf',1,'Lab04_main']]],
  ['stop_5fbalancing_20',['stop_balancing',['../Term__main_8py.html#a4763b6dbbd66b08a2a81e4382db6fc1d',1,'Term_main']]]
];
