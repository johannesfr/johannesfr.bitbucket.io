var searchData=
[
  ['tch1_0',['tch1',['../classLab03__motordriver_1_1Motor.html#a8d62159327b08b8af8341b960ee0346c',1,'Lab03_motordriver.Motor.tch1()'],['../classLab04__motordriver_1_1Motor.html#ab5a1a3ab12e72d9f8a4cb2976f60ffa1',1,'Lab04_motordriver.Motor.tch1()'],['../classTerm__motordriver_1_1Motor.html#a9546d665b6645830e294b6b798fa3efd',1,'Term_motordriver.Motor.tch1()']]],
  ['tch2_1',['tch2',['../classLab03__motordriver_1_1Motor.html#a5b06b84941fa4f37114ddea952d1b8cd',1,'Lab03_motordriver.Motor.tch2()'],['../classLab04__motordriver_1_1Motor.html#a9ad3b8b142302a31d4eb7eb2740fc8c8',1,'Lab04_motordriver.Motor.tch2()'],['../classTerm__motordriver_1_1Motor.html#a1295f1e3c8b362794a3361be5fc5ea56',1,'Term_motordriver.Motor.tch2()']]],
  ['theta_5fx_2',['theta_x',['../Term__main_8py.html#aa6823f453e838cf53a3fe6c988739147',1,'Term_main']]],
  ['theta_5fx_5fvel_3',['theta_x_vel',['../Term__main_8py.html#a3f2e20193f6df768757edc11b08a5a2d',1,'Term_main']]],
  ['theta_5fy_4',['theta_y',['../Term__main_8py.html#a82b05f01d9bbb845681427298ddffaa0',1,'Term_main']]],
  ['theta_5fy_5fvel_5',['theta_y_vel',['../Term__main_8py.html#a507ff10adfa69589c07c1d5c838fafab',1,'Term_main']]],
  ['timer_6',['timer',['../classLab03__motordriver_1_1Motor.html#a7823a1c27a40bdfeb1c479e032ec6403',1,'Lab03_motordriver.Motor.timer()'],['../classLab04__motordriver_1_1Motor.html#ace0083312b7db7d4c8a908a430f9a28a',1,'Lab04_motordriver.Motor.timer()'],['../classTerm__motordriver_1_1Motor.html#a2c0454d39273b0775e27fef6ca61746e',1,'Term_motordriver.Motor.timer()']]]
];
