var files_dup =
[
    [ "Lab00.py", "Lab00_8py.html", "Lab00_8py" ],
    [ "Lab01.py", "Lab01_8py.html", "Lab01_8py" ],
    [ "Lab02_encoder.py", "Lab02__encoder_8py.html", [
      [ "Lab02_encoder.Encoder", "classLab02__encoder_1_1Encoder.html", "classLab02__encoder_1_1Encoder" ]
    ] ],
    [ "Lab02_main.py", "Lab02__main_8py.html", "Lab02__main_8py" ],
    [ "Lab02_shares.py", "Lab02__shares_8py.html", [
      [ "Lab02_shares.Share", "classLab02__shares_1_1Share.html", "classLab02__shares_1_1Share" ],
      [ "Lab02_shares.Queue", "classLab02__shares_1_1Queue.html", "classLab02__shares_1_1Queue" ]
    ] ],
    [ "Lab02_task_encoder.py", "Lab02__task__encoder_8py.html", "Lab02__task__encoder_8py" ],
    [ "Lab02_task_user.py", "Lab02__task__user_8py.html", "Lab02__task__user_8py" ],
    [ "Lab03_encoder.py", "Lab03__encoder_8py.html", [
      [ "Lab03_encoder.Encoder", "classLab03__encoder_1_1Encoder.html", "classLab03__encoder_1_1Encoder" ]
    ] ],
    [ "Lab03_main.py", "Lab03__main_8py.html", "Lab03__main_8py" ],
    [ "Lab03_motordriver.py", "Lab03__motordriver_8py.html", [
      [ "Lab03_motordriver.DRV8847", "classLab03__motordriver_1_1DRV8847.html", "classLab03__motordriver_1_1DRV8847" ],
      [ "Lab03_motordriver.Motor", "classLab03__motordriver_1_1Motor.html", "classLab03__motordriver_1_1Motor" ]
    ] ],
    [ "Lab03_shares.py", "Lab03__shares_8py.html", [
      [ "Lab03_shares.Share", "classLab03__shares_1_1Share.html", "classLab03__shares_1_1Share" ],
      [ "Lab03_shares.Queue", "classLab03__shares_1_1Queue.html", "classLab03__shares_1_1Queue" ]
    ] ],
    [ "Lab03_task_encoder.py", "Lab03__task__encoder_8py.html", "Lab03__task__encoder_8py" ],
    [ "Lab03_task_motor.py", "Lab03__task__motor_8py.html", "Lab03__task__motor_8py" ],
    [ "Lab03_task_user.py", "Lab03__task__user_8py.html", "Lab03__task__user_8py" ],
    [ "Lab04_closedloop.py", "Lab04__closedloop_8py.html", [
      [ "Lab04_closedloop.ClosedLoop", "classLab04__closedloop_1_1ClosedLoop.html", "classLab04__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Lab04_encoder.py", "Lab04__encoder_8py.html", [
      [ "Lab04_encoder.Encoder", "classLab04__encoder_1_1Encoder.html", "classLab04__encoder_1_1Encoder" ]
    ] ],
    [ "Lab04_main.py", "Lab04__main_8py.html", "Lab04__main_8py" ],
    [ "Lab04_motordriver.py", "Lab04__motordriver_8py.html", [
      [ "Lab04_motordriver.DRV8847", "classLab04__motordriver_1_1DRV8847.html", "classLab04__motordriver_1_1DRV8847" ],
      [ "Lab04_motordriver.Motor", "classLab04__motordriver_1_1Motor.html", "classLab04__motordriver_1_1Motor" ]
    ] ],
    [ "Lab04_shares.py", "Lab04__shares_8py.html", [
      [ "Lab04_shares.Share", "classLab04__shares_1_1Share.html", "classLab04__shares_1_1Share" ],
      [ "Lab04_shares.Queue", "classLab04__shares_1_1Queue.html", "classLab04__shares_1_1Queue" ]
    ] ],
    [ "Lab04_task_encoder.py", "Lab04__task__encoder_8py.html", "Lab04__task__encoder_8py" ],
    [ "Lab04_task_motor.py", "Lab04__task__motor_8py.html", "Lab04__task__motor_8py" ],
    [ "Lab04_task_user.py", "Lab04__task__user_8py.html", "Lab04__task__user_8py" ],
    [ "Lab05_BN055.py", "Lab05__BN055_8py.html", "Lab05__BN055_8py" ],
    [ "Lab06_touchpanel.py", "Lab06__touchpanel_8py.html", [
      [ "Lab06_touchpanel.Touchpanel", "classLab06__touchpanel_1_1Touchpanel.html", "classLab06__touchpanel_1_1Touchpanel" ]
    ] ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "Term_BNO055.py", "Term__BNO055_8py.html", "Term__BNO055_8py" ],
    [ "Term_closedloop.py", "Term__closedloop_8py.html", [
      [ "Term_closedloop.ClosedLoop", "classTerm__closedloop_1_1ClosedLoop.html", "classTerm__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Term_main.py", "Term__main_8py.html", "Term__main_8py" ],
    [ "Term_motordriver.py", "Term__motordriver_8py.html", [
      [ "Term_motordriver.DRV8847", "classTerm__motordriver_1_1DRV8847.html", "classTerm__motordriver_1_1DRV8847" ],
      [ "Term_motordriver.Motor", "classTerm__motordriver_1_1Motor.html", "classTerm__motordriver_1_1Motor" ]
    ] ],
    [ "Term_shares.py", "Term__shares_8py.html", [
      [ "Term_shares.Share", "classTerm__shares_1_1Share.html", "classTerm__shares_1_1Share" ],
      [ "Term_shares.Queue", "classTerm__shares_1_1Queue.html", "classTerm__shares_1_1Queue" ]
    ] ],
    [ "Term_task_controller.py", "Term__task__controller_8py.html", "Term__task__controller_8py" ],
    [ "Term_task_datacollection.py", "Term__task__datacollection_8py.html", "Term__task__datacollection_8py" ],
    [ "Term_task_imu.py", "Term__task__imu_8py.html", "Term__task__imu_8py" ],
    [ "Term_task_motor.py", "Term__task__motor_8py.html", "Term__task__motor_8py" ],
    [ "Term_task_touchpanel.py", "Term__task__touchpanel_8py.html", "Term__task__touchpanel_8py" ],
    [ "Term_task_user.py", "Term__task__user_8py.html", "Term__task__user_8py" ],
    [ "Term_touchpanel.py", "Term__touchpanel_8py.html", [
      [ "Term_touchpanel.Touchpanel", "classTerm__touchpanel_1_1Touchpanel.html", "classTerm__touchpanel_1_1Touchpanel" ]
    ] ]
];